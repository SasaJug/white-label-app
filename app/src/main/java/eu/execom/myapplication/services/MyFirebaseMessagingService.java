package eu.execom.myapplication.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import eu.execom.myapplication.events.MessageReceivedEvent;
import eu.execom.myapplication.utils.OttoBus;


/**
 * Created by sjugurdzija on 26.9.17..
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage message){
        Log.e(TAG, "onMessageReceived: "+message.getNotification().getBody());
        OttoBus.getBus().post(new MessageReceivedEvent(message.getNotification().getBody()));
    }

}

