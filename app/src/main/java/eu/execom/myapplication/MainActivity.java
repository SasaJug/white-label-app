package eu.execom.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.TextKeyListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.squareup.otto.Subscribe;

import eu.execom.myapplication.events.MessageReceivedEvent;
import eu.execom.myapplication.utils.OttoBus;
import io.fabric.sdk.android.*;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private TextView textMessage;
    private Button button;
    private TextView buildVariant;
    private Button crashlyticsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        buildVariant = findViewById(R.id.buildVaraint);
        textMessage = findViewById(R.id.message);
        crashlyticsButton = findViewById(R.id.crashlyticsButton);
        button = findViewById(R.id.button);

        final String flavor = BuildConfig.FLAVOR;
        final String type = (BuildConfig.BUILD_TYPE);
        buildVariant.setText(flavor+Character.toUpperCase(type.charAt(0)) + type.substring(1));

        button.setText(BuildConfig.LINK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(BuildConfig.LINK));
                startActivity(i);
            }
        });

        crashlyticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Crashlytics.log("test log: "+ flavor +dimension + type);
                throw new RuntimeException(flavor+type);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        OttoBus.getBus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        OttoBus.getBus().unregister(this);
    }

    @Subscribe
    public void setMessage(final MessageReceivedEvent event){
        if(textMessage != null)
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textMessage.setText(event.getMessage());
                }
            });
    }



}
