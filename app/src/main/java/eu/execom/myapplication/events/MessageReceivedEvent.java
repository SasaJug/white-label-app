package eu.execom.myapplication.events;

/**
 * Created by sjugurdzija on 26.9.17.
 */

public class MessageReceivedEvent {

    private final String message;

    public MessageReceivedEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
