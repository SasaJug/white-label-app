package eu.execom.myapplication.utils;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by sjugurdzija on 26.9.17..
 */

public class OttoBus {

    private static Bus busInstance = new Bus(ThreadEnforcer.ANY);

    public static Bus getBus(){
        return busInstance;
    }
}
